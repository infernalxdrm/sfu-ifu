using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cameraman : MonoBehaviour
{
    LineRenderer line; //to hold the line Renderer
    public Transform target; //to hold the transform of the target
    public Transform initial;
	public Transform looker;
	
	//Объекты, хранящие элементы интерфейса
	public GameObject headPanel;
	public GameObject lowerPanel;
	public GameObject arrowsPanel;
	public GameObject centererPanel;
	public GameObject roomNumberPanel;
	public GameObject searchSuggestionPanel5;
	public GameObject searchSuggestionPanel4;
	public GameObject searchSuggestionPanel3;
	public GameObject searchSuggestionPanel2;
	public GameObject searchSuggestionPanel51;
	public GameObject searchSuggestionPanel41;
	public GameObject searchSuggestionPanel31;
	public GameObject searchSuggestionPanel21;

	
	Sprite arrowsPanelPressedUp;
	Sprite arrowsPanelPressedDown;
	Sprite arrowsPanelDefaultSprite;
	public GameObject textInitial;
	public GameObject textTarget;
	
	//Глобальные переменные для хранения состояния интерфейса
	bool g_isPanelsHidden = false;
	bool g_isPathDrawn = false;

	
    UnityEngine.AI.NavMeshAgent agent; //to hold the agent of this gameObject
	
	//Глобальные переменные для кнопок
    bool g_isForwardPressed = false;
    bool g_isBackwardPressed = false;

	List<GameObject> listOfPoints;
	

	// Первый кадр игры
    void Start()
    {
		arrowsPanelPressedUp =  Resources.Load <Sprite>("Движение наж_вперёд");
		arrowsPanelPressedDown =  Resources.Load <Sprite>("Движение наж_назад");
		arrowsPanelDefaultSprite = Resources.Load <Sprite>("Кнопки движения");
        line = GetComponent<LineRenderer>(); //get the line renderer
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>(); //get the agent
        agent.isStopped = true;
		listOfPoints = PointsGetter();
        line.SetPosition(0, transform.position);
        if(initial.position != transform.position) initial.position = transform.position;
        //getPathForward();
    }


	// Покадровый апдейт
    void Update()
    {
        if (g_isForwardPressed)
        {
            agent.isStopped = false;
            g_isBackwardPressed = false;
            getPathForward();
        }
		
        else if (g_isBackwardPressed)
        {
            agent.isStopped = false;
            g_isForwardPressed = false;
            getPathBackward();
        }

        else if (!g_isForwardPressed && !g_isBackwardPressed)
        {
            agent.isStopped = true;
        }
    }
	
	// Кнопки движения
    public void OnUpForward()
	{
		if(!g_isPathDrawn) return;
        g_isForwardPressed = false;
		arrowsPanel.GetComponent<Image>().sprite = arrowsPanelDefaultSprite;
    }

    public void OnDownForward()
	{
		if(!g_isPathDrawn) return;
        g_isForwardPressed = true;
		arrowsPanel.GetComponent<Image>().sprite = arrowsPanelPressedUp;
    }

    public void OnUpBackward()
	{
		if(!g_isPathDrawn) return;
        g_isBackwardPressed = false;
		arrowsPanel.GetComponent<Image>().sprite = arrowsPanelDefaultSprite;
    }

    public void OnDownBackward()
	{
		if(!g_isPathDrawn) return;
        g_isBackwardPressed = true;
		arrowsPanel.GetComponent<Image>().sprite = arrowsPanelPressedDown;
    }

	// Функции поиска пути
	
    void getPathForward()
    {
        agent.path.ClearCorners();
        agent.SetDestination(target.position);
		DrawPath(agent.path);
    }

    void getPathBackward()
    {
		agent.path.ClearCorners();
		agent.SetDestination(initial.position);
        DrawPath(agent.path);
    }
	
	//Прорисовка пути
    void DrawPath(UnityEngine.AI.NavMeshPath path)
    {
		line.positionCount = agent.path.corners.Length;
        line.SetPositions(path.corners);
		try
		{
			looker.position = agent.path.corners[1];
		}
		catch
		{
			return;
		}
    }
	
	public void OnClickDrawPath()
	{
		getPathForward();
		g_isPathDrawn = true;
		transform.LookAt(looker);
	}
	
	public void OnClickCenterer()
	{
		transform.LookAt(looker);
	}
	
	List<GameObject> PointsGetter()
	{
		List<GameObject> list = new List<GameObject>();
		GameObject building = GameObject.Find("/Building");
		foreach(Transform floor in building.transform)
		{
			if(floor.name == "Doors" || floor.name == "Building") continue;
			foreach(Transform room in floor)
			{
				list.Add(room.gameObject);
			}
		}
		return list;
	}
	
	public void OnTargetTextSet()
	{
		string text = textTarget.GetComponent<InputField>().text;
		if(string.IsNullOrEmpty(text))
		{
			SearchDisabler();
			return;
		}
		try
		{
			target.position = listOfPoints.Find(x => x.name == text).transform.position;
		}
		catch
		{
			roomNumberPanel.GetComponent<Text>().text = "Такого нет!";
			SearchDisabler();
			return;
		}
		if(!string.IsNullOrEmpty(text))
		{
			switch(text[1])
			{
				case '1':
					roomNumberPanel.GetComponent<Text>().text = "Г" + text + "(1 этаж)";
					break;
				case '2':
					roomNumberPanel.GetComponent<Text>().text = "Г" + text + "(2 этаж)";
					break;
				case '3':
					roomNumberPanel.GetComponent<Text>().text = "Г" + text + "(3 этаж)";
					break;
				case '4':
					roomNumberPanel.GetComponent<Text>().text = "Г" + text + "(4 этаж)";
					break;
				default:
					roomNumberPanel.GetComponent<Text>().text = "Такого нет!";
					break;
			}
		}
		SearchDisabler();
	}
	
	public void OnInitialTextSet()
	{
		string text = textInitial.GetComponent<InputField>().text;
		if(string.IsNullOrEmpty(text))
		{
			SearchDisabler();
			return;
		}
		try
		{
			initial.position = listOfPoints.Find(x => x.name == text).transform.position;
		}
		catch
		{
			roomNumberPanel.GetComponent<Text>().text = "Такого нет!";
			SearchDisabler();
			return;
		}
		transform.position = initial.position;
		SearchDisabler();
	}
	
	public void OnTargetTextEdit()
	{
		string text = textTarget.GetComponent<InputField>().text;
		if(text.Length > 0)
		{
			List<string> names = SearchHelper(text);
			GameObject panel52 = GameObject.Find("Canvas/Верхняя шторка/5i/5.2");
			GameObject panel53 = GameObject.Find("Canvas/Верхняя шторка/5i/5.3");
			GameObject panel54 = GameObject.Find("Canvas/Верхняя шторка/5i/5.4");
			GameObject panel55 = GameObject.Find("Canvas/Верхняя шторка/5i/5.5");
			GameObject panel42 = GameObject.Find("Canvas/Верхняя шторка/4i/4.2");
			GameObject panel43 = GameObject.Find("Canvas/Верхняя шторка/4i/4.3");
			GameObject panel44 = GameObject.Find("Canvas/Верхняя шторка/4i/4.4");
			GameObject panel32 = GameObject.Find("Canvas/Верхняя шторка/3i/3.2");
			GameObject panel33 = GameObject.Find("Canvas/Верхняя шторка/3i/3.3");
			GameObject panel22 = GameObject.Find("Canvas/Верхняя шторка/2i/2.2");
			if(names.Count >= 4)
			{
				SearchDisabler();
				searchSuggestionPanel51.SetActive(true);
				panel52.GetComponent<Text>().text = names[0];
				panel53.GetComponent<Text>().text = names[1];
				panel54.GetComponent<Text>().text = names[2];
				panel55.GetComponent<Text>().text = names[3];
			}
			else if (names.Count == 3)
			{
				SearchDisabler();
				searchSuggestionPanel41.SetActive(true);
				panel42.GetComponent<Text>().text = names[0];
				panel43.GetComponent<Text>().text = names[1];
				panel44.GetComponent<Text>().text = names[2];
			}
			else if (names.Count == 2)
			{
				SearchDisabler();
				searchSuggestionPanel31.SetActive(true);
				panel32.GetComponent<Text>().text = names[0];
				panel33.GetComponent<Text>().text = names[1];
			}	
			else if (names.Count == 1)
			{
				SearchDisabler();
				searchSuggestionPanel21.SetActive(true);
				panel22.GetComponent<Text>().text = names[0];
			}
			else
			{
				SearchDisabler();
				return;
			}	
		}	
	}
	
	public void OnInitialTextEdit()
	{
		string text = textInitial.GetComponent<InputField>().text;
		
		if(text.Length > 0)
		{
			List<string> names = SearchHelper(text);
			GameObject panel52 = GameObject.Find("Canvas/Верхняя шторка/5v/5.2");
			GameObject panel53 = GameObject.Find("Canvas/Верхняя шторка/5v/5.3");
			GameObject panel54 = GameObject.Find("Canvas/Верхняя шторка/5v/5.4");
			GameObject panel55 = GameObject.Find("Canvas/Верхняя шторка/5v/5.5");
			GameObject panel42 = GameObject.Find("Canvas/Верхняя шторка/4v/4.2");
			GameObject panel43 = GameObject.Find("Canvas/Верхняя шторка/4v/4.3");
			GameObject panel44 = GameObject.Find("Canvas/Верхняя шторка/4v/4.4");
			GameObject panel32 = GameObject.Find("Canvas/Верхняя шторка/3v/3.2");
			GameObject panel33 = GameObject.Find("Canvas/Верхняя шторка/3v/3.3");
			GameObject panel22 = GameObject.Find("Canvas/Верхняя шторка/2v/2.2");
			if(names.Count >= 4)
			{
				SearchDisabler();
				searchSuggestionPanel5.SetActive(true);
				panel52.GetComponent<Text>().text = names[0];
				panel53.GetComponent<Text>().text = names[1];
				panel54.GetComponent<Text>().text = names[2];
				panel55.GetComponent<Text>().text = names[3];
			}
			else if (names.Count == 3)
			{
				SearchDisabler();
				searchSuggestionPanel4.SetActive(true);
				panel42.GetComponent<Text>().text = names[0];
				panel43.GetComponent<Text>().text = names[1];
				panel44.GetComponent<Text>().text = names[2];
			}
			else if (names.Count == 2)
			{
				SearchDisabler();
				searchSuggestionPanel3.SetActive(true);
				panel32.GetComponent<Text>().text = names[0];
				panel33.GetComponent<Text>().text = names[1];
			}	
			else if (names.Count == 1)
			{
				SearchDisabler();
				searchSuggestionPanel2.SetActive(true);
				panel22.GetComponent<Text>().text = names[0];
			}
			else
			{
				SearchDisabler();
				return;
			}	
		}	
	}
	
	public void OnInitialSearchClick(Button button)
	{
		Debug.Log("debugl ol");
		textInitial.GetComponent<InputField>().text = button.GetComponentInChildren<Text>().text;
	}
	
	public void OnTargetSearchClick(Button button)
	{
		textTarget.GetComponent<InputField>().text = button.GetComponentInChildren<Text>().text;
	}
	
	void SearchDisabler()
	{
		searchSuggestionPanel5.SetActive(false);
		searchSuggestionPanel4.SetActive(false);
		searchSuggestionPanel3.SetActive(false);
		searchSuggestionPanel2.SetActive(false);
		searchSuggestionPanel51.SetActive(false);
		searchSuggestionPanel41.SetActive(false);
		searchSuggestionPanel31.SetActive(false);
		searchSuggestionPanel21.SetActive(false);
	}
	
	List<string> SearchHelper(string text)
	{
		List<string> list = new List<string>();
		foreach(GameObject point in listOfPoints)
		{
			if(point.name.Contains(text))
				list.Add(point.name);
		}
		return list;
	}
	
	public void OnClickSlidePanel()
	{	
		if(!headPanel.GetComponent<Animator>().enabled && !headPanel.GetComponent<Animator>().enabled)
		{
			headPanel.GetComponent<Animator>().enabled = true;
			lowerPanel.GetComponent<Animator>().enabled = true;
			arrowsPanel.GetComponent<Animator>().enabled = true;
			centererPanel.GetComponent<Animator>().enabled = true;
			g_isPanelsHidden = true;
		}
		else
		{
			if(!g_isPanelsHidden)
			{
				headPanel.GetComponent<Animator>().SetTrigger("remove");
				lowerPanel.GetComponent<Animator>().SetTrigger("remove");
				arrowsPanel.GetComponent<Animator>().SetTrigger("remove");
				centererPanel.GetComponent<Animator>().SetTrigger("remove");
				g_isPanelsHidden = true;
				return;
			}
			else
			{
				headPanel.GetComponent<Animator>().SetTrigger("restore");
				lowerPanel.GetComponent<Animator>().SetTrigger("restore");
				arrowsPanel.GetComponent<Animator>().SetTrigger("restore");
				centererPanel.GetComponent<Animator>().SetTrigger("restore");
				g_isPanelsHidden = false;
				return;
			}
		}
	}
}
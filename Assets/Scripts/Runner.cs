using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Runner : MonoBehaviour
{
	UnityEngine.AI.NavMeshAgent agent;
	public Transform initial;
	public Transform target;
	public Transform cameraman;
	public GameObject textTarget;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>(); //get the agent
    }

    // Update is called once per frame
    void Update()
    {
		try
		{
			agent.SetDestination(GameObject.Find($"Building/{textTarget.GetComponent<Text>().text}").transform.position);
		}
		catch
		{
			return;
		}
		
		string text = textTarget.GetComponent<Text>().text;
		agent.SetDestination(GameObject.Find($"Building/{text}").transform.position);
        if(transform.position == target.position)
		{
			agent.SetDestination(GameObject.Find($"Building/{text}").transform.position);
			transform.position = cameraman.position;
		}
    }
}
